# Posts on neurophysiology

Repo for Impulse Neiry posts sources.

## Setup

Create new Python environment (via virtualenv or conda), then install dependencies:\
`pip install -r requirements.txt`

Alternatively (advanced track) you could use [poetry](https://python-poetry.org/docs/):\
`poetry install`

### Note for Macs on M1

You will need to install `hdf5` core library manually ([source](https://stackoverflow.com/questions/66741778/how-to-install-h5py-needed-for-keras-on-macos-with-m1)):

```bash
brew install hdf5
export HDF5_DIR=/opt/homebrew/Cellar/hdf5/<your_version>
```

Then follow regular setup instructions.
